using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class TouchInput : MonoBehaviour
{

    public Camera arCamera;
    public Material selectedMat;
    public Material DoubleClickedMat;
    public float TimeBetweenClick =0.5f;

    public float RotationSpeed =1;
    private float PassedTimeClick;
    public int ClickedObjectID;
    private Material BaseMat;
    private Material SelectedBaseMat;

    private bool movingObject = false;
    private bool DoubleClicked = false;
    private GameObject Placeableobject;
    private GameObject LastSelectedPrefab;


    [SerializeField] private GameObject m_prefab;
    private ARRaycastManager m_arRaycastManager;
    // Start is called before the first frame update
    void Start()
    {
        m_arRaycastManager = FindObjectOfType<ARRaycastManager>();
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log("down : " + Input.GetMouseButtonDown(0));
        //Debug.Log("up : " +Input.GetMouseButtonUp(0));
        //Debug.Log("hold ?"+Input.GetMouseButton(0));
        PassedTimeClick += Time.deltaTime;
        Debug.Log(PassedTimeClick);


        if (Input.GetMouseButtonDown(0))
        {

            if (!IsOverUi(Input.mousePosition))
            {
                Ray ray = arCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject))
                {
                    Debug.Log(hitObject);
                    if (hitObject.transform.tag == "Placeable")
                    {
                        movingObject = true;
                        Placeableobject = hitObject.transform.gameObject;
                        BaseMat = Placeableobject.GetComponent<MeshRenderer>().material;
                        Debug.Log(BaseMat.name);
                        Placeableobject.GetComponent<MeshRenderer>().material = selectedMat;
                        Debug.Log("object selected");
                        Debug.Log(ClickedObjectID);
                        
                        if (PassedTimeClick < TimeBetweenClick && ClickedObjectID == Placeableobject.gameObject.GetInstanceID())
                        {
                            DoubleClicked =true;
                            Debug.Log("double clicked");
                            Placeableobject.GetComponent<MeshRenderer>().material = DoubleClickedMat;
                        }else{
                            PassedTimeClick=0;
                        }

                        ClickedObjectID = Placeableobject.gameObject.GetInstanceID();
                        

                    }
                }

                var hits = new List<ARRaycastHit>();
                var touchPositionOnScreen = Input.mousePosition;

                if (m_arRaycastManager.Raycast(touchPositionOnScreen, hits, TrackableType.PlaneWithinPolygon) && !movingObject)
                {

                    var spawnPosition = hits[0].pose.position;
                    SpawnCubeAtPosition(spawnPosition);

                }
            }
            else
            {
                Debug.Log("On UI");
                Ray ray = arCamera.ScreenPointToRay(Input.mousePosition);
                int LayerMask = 1<<5;
                RaycastHit hitObject;
                if (Physics.Raycast(ray, out hitObject,1000,LayerMask, QueryTriggerInteraction.Collide))
                {
                    if (hitObject.transform.tag == "SelectablePrefab")
                    {
                        if (LastSelectedPrefab !=null && LastSelectedPrefab.name != hitObject.transform.gameObject.name)
                        {
                            m_prefab = hitObject.transform.gameObject;
                            LastSelectedPrefab.GetComponent<MeshRenderer>().material = SelectedBaseMat;
                            LastSelectedPrefab = hitObject.transform.gameObject;
                            SelectedBaseMat = hitObject.transform.gameObject.GetComponent<MeshRenderer>().material;
                            hitObject.transform.gameObject.GetComponent<MeshRenderer>().material = selectedMat;
                            Debug.Log("object selected from list");
                        }else if(LastSelectedPrefab==null)
                        {
                            m_prefab = hitObject.transform.gameObject;
                            LastSelectedPrefab = hitObject.transform.gameObject;
                            SelectedBaseMat = hitObject.transform.gameObject.GetComponent<MeshRenderer>().material;
                            hitObject.transform.gameObject.GetComponent<MeshRenderer>().material = selectedMat;
                            Debug.Log("object selected from list");
                        }else{
                            Debug.Log("object already selected from list");
                        }

                    }else{
                        Debug.Log(hitObject.transform.gameObject.name);
                    }
                }else{
                    Debug.Log("hit nothing");
                }

            }

        }
        if (Input.GetMouseButtonUp(0) && Placeableobject != null)
        {
            movingObject = false;
            DoubleClicked =false;
            Placeableobject.GetComponent<MeshRenderer>().material = BaseMat;
            Placeableobject = null;

            Debug.Log("releasing object");
        }

        if (Input.GetMouseButton(0) && movingObject && Placeableobject != null && !DoubleClicked)
        {
            Debug.Log("moving object");
            var hits = new List<ARRaycastHit>();
            var touchPositionOnScreen = Input.mousePosition;

            if (m_arRaycastManager.Raycast(touchPositionOnScreen, hits, TrackableType.PlaneWithinPolygon))
            {
                Placeableobject.transform.position = hits[0].pose.position;
            }

        }

        if (Input.GetMouseButton(0) && movingObject && Placeableobject != null && DoubleClicked)
        {
            Debug.Log("rotating object");
            Vector2 rotation = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
            rotation*= RotationSpeed;

            Placeableobject.transform.Rotate(-Vector3.up, rotation.x, Space.World);
            Placeableobject.transform.Rotate(arCamera.transform.right, rotation.y, Space.World);

        }

    }

    private void SpawnCubeAtPosition(Vector3 _spawnPosition)
    {
       var props =  Instantiate(m_prefab, _spawnPosition, Quaternion.identity);
       if (SelectedBaseMat !=null)
       {
        props.GetComponent<MeshRenderer>().material = SelectedBaseMat;
       }
       props.tag ="Placeable";
    }

    private bool IsOverUi(Vector3 pos)
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return true;
        }

        PointerEventData eventPosition = new PointerEventData(EventSystem.current);
        eventPosition.position = new Vector2(pos.x,pos.y);

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventPosition, results);
        Debug.Log(results.Count);

        return results.Count > 0;
    }
}
