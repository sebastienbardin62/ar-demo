# AR Demo

## Description
Small project to test ARCore posibilities. In this mobile app you can add 3D shapes to your Augmented reality scene, you can move them and rotate them with just one finger. Adding simple shapes to an augmented reality scene isn't very impressive but, for example you can replace those shapes by 3D models of furnitures or paintings and you will get a perfect interior design app ! 

## Futur of the project
In the near future I would like to add a built-in interface to upload your own 3D models. In addition I would like to add the possibilty to scan objects thanks to LiDAR.


## License
This project is open source. If you want to use it just credit me.


